// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	l4g "github.com/alecthomas/log4go"
	"github.com/spf13/cobra"

	"gitlab.com/alpha/alpha-server/api"
	"gitlab.com/alpha/alpha-server/app"
	"gitlab.com/alpha/alpha-server/utils"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:    "server",
	Hidden: true,
	Short:  "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	RunE: runServerCmd,
}

func init() {
	rootCmd.AddCommand(serverCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func runServerCmd(cmd *cobra.Command, args []string) error {
	config, err := cmd.Flags().GetString("config")
	if err != nil {
		return err
	}

	runServer(config)
	return nil
}

func runServer(config string) {
	options := []app.Option{app.ConfigFile(config)}
	a, _ := app.NewApp(options...)

	if utils.TranslationsPreInit() != nil {
		l4g.Error("error")
	}

	api.Init(a, a.Srv.Router)
	a.InitStores()
	a.StartServer()
}
