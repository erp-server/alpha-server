package store

import "gopkg.in/mgo.v2/bson"

// DBStore interface
type DBStore interface {
	// TODO: Create NOSQL(mongoDB) DBStore Interface and move all
	// TODO: below database related function in it take reference of provider interface for more.
	IsCollectionExist(string) bool
	CreateDocument(string, interface{}) error
	Search(string, interface{}, int, int) ([]bson.M, error)
	DeleteDocument(string, string) error
	GetDocumentByID(string, bson.ObjectId) (bson.M, error)
	UpdateDocByID(string, bson.ObjectId, interface{}) error
	FindOne(string, interface{}) (bson.M, error)
	DropCollection(string) (bool, error)

	Session() SessionsStore
	Item() ItemStore
	List() ListStore
	User() UserStore
	Provider() ProviderStore
}
