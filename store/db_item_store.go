package store

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

// DBItemStore struct
type DBItemStore struct {
	DBStore
}

// NewDBItemStore function create the item store.
func NewDBItemStore(dbStore DBStore) ItemStore {
	s := &DBItemStore{dbStore}
	return s
}

// Create function is create the item in given collection.
func (s *DBItemStore) Create(collection string, props *model.Props) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		props.Id = bson.NewObjectId()
		if err := s.DBStore.CreateDocument(collection, props); err != nil {
			result.Err = model.NewAppError("DBItemStore.Save", "store.db_item.save.app_error", nil, "id="+props.Id.Hex()+", "+err.Error(), http.StatusBadRequest)
		}
		result.Data = props
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

// Update function will update the item in given collection.
func (s *DBItemStore) Update(collection string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)

	return storeChannel
}

// Delete function will Delete the item in given collection.
func (s *DBItemStore) Delete(collection string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if err := s.DBStore.DeleteDocument(collection, query.(string)); err != nil {
			result.Err = model.NewAppError("DBItemStore.Delete", "store.db_item.delete.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = &model.SuccessResponse{
				Success: true,
			}
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// ReadAll function used to get the all items from the given collection.
func (s *DBItemStore) ReadAll(collection string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if items, err := s.DBStore.Search(collection, query, 0, 0); err != nil {
			result.Err = model.NewAppError("DBItemStore.Read", "store.db_item.read.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = model.PropsFromBson(items)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Read function used to get the item from the given collection.
func (s *DBItemStore) Read(collection string, id bson.ObjectId) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if item, err := s.DBStore.GetDocumentByID(collection, id); err != nil {
			result.Err = model.NewAppError("DBItemStore.Read", "store.db_item.read.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = model.PropFromBson(item)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
