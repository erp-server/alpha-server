package store

// IProviderStore struct
type IProviderStore struct {
	DBStore
	MsgStore
}

// NewProviderStore function create the instance of IProviderStore.
func NewProviderStore(store DBStore) ProviderStore {
	supplier := &IProviderStore{store, NewMsgStore(store)}
	return supplier
}

// MessageStoreProvider function return the MsgStore.
func (i *IProviderStore) MessageStoreProvider() MsgStore {
	return i.MsgStore
}
