package store

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

// DBUserStore struct
type DBUserStore struct {
	DBStore
}

// NewDBUserStore function create the new instance of DBUserStore 
func NewDBUserStore(dbStore DBStore) UserStore {
	s := &DBUserStore{dbStore}
	return s
}

// Create function create the new user.
func (u *DBUserStore) Create(user *model.User) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		user.PreSave()
		u.Provider()
		var query = bson.M{"username": user.PR_USERNAME, "email": user.PR_EMAIL_ADDRESS}
		if userProps, err := u.DBStore.Search(model.DB_USER_TABLE, query, 0, 1); err != nil {
			result.Err = model.NewAppError("DBUsersStore.Create", "store.db_user.duplicate_check.app_error", nil, "username="+user.PR_USERNAME+", "+err.Error(), http.StatusBadRequest)
		} else {
			if len(userProps) != 0 {
				result.Err = model.NewAppError("DBUsersStore.Create", "store.db_user.user.exists.app_error", map[string]interface{}{"User": user.PR_USERNAME}, "username="+user.PR_USERNAME, http.StatusBadRequest)
			} else {
				if err := u.DBStore.CreateDocument(model.DB_USER_TABLE, user); err != nil {
					result.Err = model.NewAppError("DBUsersStore.Create", "store.db_user.save.app_error", nil, "id="+user.Id.Hex()+", "+err.Error(), http.StatusBadRequest)
				} else {
					go u.Provider().MessageStoreProvider().createMessageStore(user)
				}
			}
		}

		result.Data = user
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

// GetForLogin function used to retraive the user information from either
// username or query
//
// loginID String
// query Interface
func (u *DBUserStore) GetForLogin(loginID string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)

	go func() {
		result := StoreResult{}
		if query == nil {
			query = bson.M{"username": loginID}
		}

		if userProps, err := u.DBStore.FindOne(model.DB_USER_TABLE, query); err != nil {
			result.Err = model.NewAppError("DBUsersStore.GetForLogin", "store.db_user.get_for_login.error.app_error", nil, "loginId="+loginID+", "+err.Error(), http.StatusBadRequest)
		} else {
			result.Data = model.UserFromBSON(userProps)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Update function used to update the record
func (u *DBUserStore) Update(user *model.User) StoreChannel {
	storeChannel := make(StoreChannel, 1)

	go func() {
		result := StoreResult{}
		if err := u.DBStore.UpdateDocByID(model.DB_USER_TABLE, user.Id, user); err != nil {
			result.Err = model.NewAppError("DBUsersStore.UpdateUser", "store.db_user.update_user.error.app_error", nil, "userId="+user.Id.Hex()+", "+err.Error(), http.StatusBadRequest)
		}
		result.Data = user
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Read used to read the data from the backend.
func (u *DBUserStore) Read(query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if usersResult, err := u.DBStore.Search(model.DB_USER_TABLE, query, 0, 0); err != nil {
			result.Err = model.NewAppError("DBUsersStore.Read", "store.db_user.list_users.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = model.UsersFromBson(usersResult)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Delete function delete the record
func (u *DBUserStore) Delete(ID string) StoreChannel {
	return u.Item().Delete(model.DB_USER_TABLE, ID)
}

// GetContexts function used to retrive the context.
func (u *DBUserStore) GetContexts(user *model.User) StoreChannel {
	result := <-u.Provider().MessageStoreProvider().getPluginRootFolder(user)
	if result.Err != nil {
		return nil
	}
	pluginRootFolder := result.Data.(*model.Props)
	var query interface{} = bson.M{
		"parent_entryid": pluginRootFolder.PR_ENTRYID,
	}
	return u.List().Read(model.OBJECTS_TABLE, query)
}

// GetUserByID function
func (u *DBUserStore) GetUserByID(ID string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if userProps, err := u.DBStore.GetDocumentByID(model.DB_USER_TABLE, bson.ObjectIdHex(ID)); err != nil {
			result.Err = model.NewAppError("DBUsersStore.GetForLogin", "store.db_user.get_for_login.error.app_error", nil, "userid="+ID+", "+err.Error(), http.StatusBadRequest)
		} else {
			user := model.UserFromBSON(userProps)
			user.Sensitized()
			result.Data = user
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
