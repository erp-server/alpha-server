package store

import (
	"fmt"
	"log"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

type IMsgStore struct {
	DBStore
}

func NewMsgStore(store DBStore) MsgStore {
	initMsgStore()
	return &IMsgStore{store}
}

func initMsgStore() {

}

func (t *IMsgStore) createEntryId(id *model.EntryId) string {
	id.PreSave()
	id.IsValid()
	return id.Flags + id.ProviderGUID + id.FolderType + id.GlobeCounter + id.DatabaseGUID + id.Pad
}

func (t *IMsgStore) createStoreEntryId(storeEntryId *model.StoreEntryId) string {
	storeEntryId.PreSave()
	return storeEntryId.Flags + storeEntryId.ProviderGUID + storeEntryId.Version + storeEntryId.Flag + storeEntryId.WrappedFlags + storeEntryId.WrappedProviderUID + storeEntryId.WrappedType
}

// createMessageStore function called only when new user has been created
// it will create user store as well as necessary plugins root folders.
func (t *IMsgStore) createMessageStore(user *model.User) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		props := model.NewProps()
		props.Id = bson.NewObjectId()
		props.PR_STORE_ENTRYID = t.createStoreEntryId(&model.StoreEntryId{
			WrappedProviderUID: props.Id.Hex(),
		})
		props.PR_ENTRYID = props.PR_STORE_ENTRYID
		props.PR_DISPLAY_NAME = fmt.Sprintf("%s %s", "Inbox - ", user.PR_DISPLAYNAME)
		props.PR_MDB_PROVIDER = user.Id.Hex() //storeObjId.Hex()
		props.PR_STORE_RECORD_KEY = props.Id.Hex()
		props.PR_RECORD_KEY = props.PR_STORE_RECORD_KEY
		props.PR_OBJECT_TYPE = model.MAPI_STORE
		props.PR_MAPPING_SIGNATURE = props.PR_STORE_RECORD_KEY

		if err := t.CreateDocument(model.OBJECTS_TABLE, props); err != nil {
			result.Err = model.NewAppError("NewMsgStore.createMessageStore", "store.message_store.create.object_table.app_error", nil, "user_id"+user.Id.Hex()+"="+err.Error(), http.StatusBadRequest)
		}

		if result = <-t.createRootContainer(props); result.Err != nil {
			result.Err = model.NewAppError("NewMsgStore.createMessageStore", "store.message_store.create.root_container.app_error", nil, "user_id"+user.Id.Hex(), http.StatusBadRequest)
		}

		result.Data = &model.SuccessResponse{Success: result.Err == nil}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) createRootContainer(msgStore *model.Props) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		props := model.NewProps()
		props.PR_FOLDER_TYPE = model.FOLDER_ROOT
		props.PR_SUBFOLDERS = true
		if entryid, err := t.CreateFolder(props, msgStore, msgStore); err != nil {
			log.Println("Error : IPM Subtree is not created properly", err)
		} else {
			props.PR_PARENT_ENTRYID = entryid
		}

		// we need to remove id property from props
		// as we use that props to update record.
		id := props.Id
		props.Id = ""
		if err := t.UpdateDocByID(model.OBJECTS_TABLE, id, props); err != nil {
			result.Err = model.NewAppError("NewMsgStore.createRootContainer", "store.message_store.update.root_container.app_error", nil, "", http.StatusBadRequest)
		}

		result = <-t.createDefaultFolders(props)
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) createDefaultFolders(parent *model.Props) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if result = <-t.createIPMPluginsRoot(parent); result.Err != nil {
			result.Err = model.NewAppError("NewMsgStore.createDefaultFolders", "store.message_store.create.ipm_subtree.app_error", nil, "", http.StatusBadRequest)
		}
		result.Data = model.SuccessResponse{Success: result.Err == nil}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) createIPMPluginsRoot(parent *model.Props) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		props := model.NewProps()
		props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
		props.PR_FOLDER_TYPE = model.FOLDER_GENERIC
		props.PR_DISPLAY_NAME = "PLUGINS_ROOT"
		props.PR_SUBFOLDERS = true

		result = <-t.GetMsgStoreObj(parent)
		msgStore := result.Data.(*model.Props)
		entryid, err := t.CreateFolder(props, parent, msgStore)
		if err != nil {
			log.Println("Error : IPM Subtree is not created properly", err)
		}
		msgStore.PR_IPM_PLUGINS_ROOT_ENTRYID = entryid
		id := msgStore.Id
		msgStore.Id = ""

		// Update message store document in object store.
		if err := t.UpdateDocByID(model.OBJECTS_TABLE, id, msgStore); err != nil {
			log.Println("ERROR : Could not save the entry to MongoDB:", err)
		}
		if err := t.createCRMRootFolder(props); err != nil {
			log.Println("Error : Deleted items folder is not created properly", err)
		}
	}()
	return storeChannel
}

func (t *IMsgStore) createCRMRootFolder(parent *model.Props) error {
	props := model.NewProps()
	props.PR_PARENT_ENTRYID = parent.PR_ENTRYID
	props.PR_FOLDER_TYPE = model.FOLDER_GENERIC
	props.PR_DISPLAY_NAME = "CRM"
	result := <-t.GetMsgStoreObj(parent)
	msgStore := result.Data.(*model.Props)
	entryid, err := t.CreateFolder(props, parent, msgStore)
	if err != nil {
		log.Println("Error : IPM Subtree is not created properly", err)
	}
	msgStore.PR_IPM_CRM_ENTRYID = entryid
	id := msgStore.Id
	msgStore.Id = ""

	// Update message store document in object store.
	if err := t.UpdateDocByID(model.OBJECTS_TABLE, id, msgStore); err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	return err
}

func (t *IMsgStore) getPluginRootFolder(user *model.User) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if result = <-t.getDefaultMessageStore(user); result.Err == nil {
			defaultMsgStore := result.Data.(*model.Props)
			if result = <-t.getByEntryId(defaultMsgStore.PR_IPM_PLUGINS_ROOT_ENTRYID); result.Err != nil {
				result.Err = model.NewAppError("NewMsgStore.getPluginRootFolder", "store.message_store.get.plugin_root.app_error", nil, "user: "+user.PR_FIRSTNAME, http.StatusBadRequest)
			}
		} else {
			result.Err = model.NewAppError("NewMsgStore.getPluginRootFolder", "store.message_store.get.default_message_store.app_error", nil, "user: "+user.PR_FIRSTNAME, http.StatusBadRequest)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) getByEntryId(entryid string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		var query interface{} = bson.M{
			"entryid": entryid,
		}
		log.Println(entryid)
		folder, err := t.FindOne(model.OBJECTS_TABLE, query)
		log.Println(folder)
		if err != nil {
			result.Err = model.NewAppError("NewMsgStore.getFolderByEntryId", "store.message_store.get.folder.app_error", nil, "folder_id : "+entryid, http.StatusBadRequest)
		}

		result.Data = model.PropFromBson(folder)
		log.Println(result.Data)
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) getDefaultMessageStore(user *model.User) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		var queryToFindDefaultStore interface{} = bson.M{
			"mdb_provider": user.Id.Hex(),
			"object_type":  model.MAPI_STORE,
		}
		msgStoreProps, err := t.FindOne(model.OBJECTS_TABLE, queryToFindDefaultStore)
		if err != nil {
			result.Err = model.NewAppError("NewMsgStore.getDefaultMessageStore", "store.message_store.get.default_message.app_error", nil, "user_id : "+user.Id.Hex()+" display_name : "+user.PR_DISPLAYNAME, http.StatusBadRequest)
		}
		result.Data = model.PropFromBson(msgStoreProps)
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (t *IMsgStore) GetMsgStoreObj(folderProps *model.Props) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		var query interface{} = bson.M{
			"store_entryid": folderProps.PR_STORE_ENTRYID,
			"object_type":   model.MAPI_STORE,
		}

		msgStoreProps, err := t.FindOne(model.OBJECTS_TABLE, query)
		if err != nil {
			result.Err = model.NewAppError("NewMsgStore.getDefaultMessageStore", "store.message_store.get.default_message.app_error", nil, "store_entryid : "+folderProps.PR_STORE_ENTRYID, http.StatusBadRequest)
		}

		result.Data = model.PropFromBson(msgStoreProps)
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel

}

// Function which create the folder record in backend.
func (t *IMsgStore) CreateFolder(props *model.Props, parent *model.Props, store *model.Props) (string, error) {
	props.Id = bson.NewObjectId()
	props.PR_OBJECT_TYPE = model.MAPI_FOLDER
	props.PR_STORE_ENTRYID = parent.PR_STORE_ENTRYID
	props.PR_MDB_PROVIDER = parent.PR_MDB_PROVIDER
	props.PR_MAPPING_SIGNATURE = parent.PR_MAPPING_SIGNATURE
	props.PR_STORE_RECORD_KEY = parent.PR_MAPPING_SIGNATURE
	if len(props.PR_ENTRYID) == 0 {
		// generate Entryid for default folders
		props.PR_ENTRYID = t.createEntryId(&model.EntryId{
			ProviderGUID: store.Id.Hex(),
			DatabaseGUID: props.Id.Hex(),
		})
	}

	props.PR_RECORD_KEY = props.PR_ENTRYID
	// Create document for the default folder in Object table
	err := t.CreateDocument(model.OBJECTS_TABLE, props)
	if err != nil {
		log.Println("ERROR : Could not save the entry to MongoDB:", err)
	}
	return props.PR_ENTRYID, err
}
