package store

import (
	"crypto/tls"
	"net"
	"time"

	l4g "github.com/alecthomas/log4go"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

// DBSupplierOldStores struct
type DBSupplierOldStores struct {
	itemStore     ItemStore
	listStore     ListStore
	sessionsStore SessionsStore
	userStore     UserStore
	providerStore ProviderStore
}

// DBSupplier struct
type DBSupplier struct {
	oldStores  DBSupplierOldStores
	dbSettings *model.DataBaseSettings
}

// NewDBSupplier create new instance of DBSupplier.
func NewDBSupplier(config *model.DataBaseSettings) *DBSupplier {
	supplier := &DBSupplier{
		dbSettings: config,
	}
	supplier.oldStores.itemStore = NewDBItemStore(supplier)
	supplier.oldStores.listStore = NewDBListStore(supplier)
	supplier.oldStores.sessionsStore = NewDBSessionStore(supplier)
	supplier.oldStores.userStore = NewDBUserStore(supplier)
	supplier.oldStores.providerStore = NewProviderStore(supplier)

	dbSettings = config

	return supplier
}

var (
	mgoSession   *mgo.Session
	databaseName = "erp"
	dbSettings   *model.DataBaseSettings
)

// Function used to get the MongoDB session object
func (s *DBSupplier) getSession() *mgo.Session {
	if mgoSession == nil {
		//TODO: Check the *dataSource.DataSourceAtlas if true then go for atlas connection else go for local db connection.
		dataSource := dbSettings.DataSource
		databaseName = *dbSettings.DatabaseName
		DialInfo := &mgo.DialInfo{
			Addrs:    *dataSource.ReplicaSets,
			Database: *dbSettings.DatabaseName,
			Username: *dataSource.UserName,
			Password: *dataSource.Password,
			Source:   *dataSource.Source,
		}
		tlsConfig := &tls.Config{}
		DialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			return tls.Dial("tcp", addr.String(), tlsConfig)
		}

		DialInfo.Timeout = 15 * time.Second

		session, err := mgo.DialWithInfo(DialInfo)
		if err != nil {
			l4g.Error(err)
		}
		mgoSession = session
	}

	return mgoSession.Clone()
}

// withDatabase function perform an action on given collection.
func (s *DBSupplier) withCollection(collection string, fn func(*mgo.Collection) error) error {
	session := s.getSession()
	defer session.Close()
	c := session.DB(databaseName).C(collection)
	return fn(c)
}

// withDatabase function perform an action on given database.
func (s *DBSupplier) withDatabase(fn func(*mgo.Database) error) error {
	session := s.getSession()
	defer session.Close()
	db := session.DB(databaseName)
	return fn(db)
}

// CreateDocument function was create the new document record in
// given collection.
func (s *DBSupplier) CreateDocument(collectionName string, props interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.Insert(props)
		return fn
	}
	insert := func() error {
		return s.withCollection(collectionName, query)
	}
	return insert()
}

// DeleteDocument function was delete the document record from
// given collection.
func (s *DBSupplier) DeleteDocument(collectionName string, id string) error {
	query := func(c *mgo.Collection) error {
		fn := c.RemoveId(bson.ObjectIdHex(id))
		return fn
	}
	delete := func() error {
		return s.withCollection(collectionName, query)
	}
	return delete()
}

// GetDocumentByID function get the
func (s *DBSupplier) GetDocumentByID(collection string, id bson.ObjectId) (document bson.M, err error) {
	query := func(c *mgo.Collection) error {
		fn := c.FindId(id).One(&document)
		return fn
	}
	find := func() error {
		return s.withCollection(collection, query)
	}
	if err := find(); err != nil {
		return nil, err
	}

	return document, nil
}

// UpdateDocByID function update the document by given id
func (s *DBSupplier) UpdateDocByID(collection string, id bson.ObjectId, data interface{}) error {
	query := func(c *mgo.Collection) error {
		fn := c.UpdateId(id, data)
		return fn
	}
	updateByID := func() error {
		return s.withCollection(collection, query)
	}

	return updateByID()
}

// Search function search the document based on given search query.
func (s *DBSupplier) Search(collectionName string, q interface{}, skip int, limit int) (searchResults []bson.M, e error) {
	query := func(c *mgo.Collection) error {
		fn := c.Find(q).Skip(skip).Limit(limit).All(&searchResults)
		if limit < 0 {
			fn = c.Find(q).Skip(skip).All(&searchResults)
		}
		return fn
	}
	search := func() error {
		return s.withCollection(collectionName, query)
	}
	err := search()
	if err != nil {
		e = err
	}
	return
}

// FindOne function find the first maching document in collection based on given query.
func (s *DBSupplier) FindOne(collectionName string, q interface{}) (result bson.M, e error) {
	query := func(c *mgo.Collection) error {
		fn := c.Find(q).One(&result)
		return fn
	}
	find := func() error {
		return s.withCollection(collectionName, query)
	}
	err := find()
	if err != nil {
		e = err
	}
	return
}

// IsCollectionExist function check that collection is exist in given database.
func (s *DBSupplier) IsCollectionExist(collectionName string) (isExist bool) {
	query := func(db *mgo.Database) error {
		collections, _ := db.CollectionNames()
		isExist = false
		for _, collection := range collections {
			if collection == collectionName {
				isExist = true
				break
			}
		}
		return nil
	}

	_ = s.withDatabase(query)
	return
}

// DropCollection function used to drop the collection from database.
func (s *DBSupplier) DropCollection(collectionName string) (bool, error) {
	query := func(db *mgo.Database) error {
		return db.C(collectionName).DropCollection()
	}

	if err := s.withDatabase(query); err != nil {
		return false, err
	}
	return true, nil
}

// GetGlobeCounter used to get the total number of record on Object
// collection in mongoDB
func (s *DBSupplier) GetGlobeCounter(collectionName string) (count int, err error) {
	query := func(db *mgo.Database) error {
		count, err = db.C(collectionName).Count()
		return err
	}
	if err := s.withDatabase(query); err != nil {
		return 0, err
	}
	return count, nil
}

// Session function returns the session store object.
func (s *DBSupplier) Session() SessionsStore {
	return s.oldStores.sessionsStore
}

// Item function returns the item store object.
func (s *DBSupplier) Item() ItemStore {
	return s.oldStores.itemStore
}

// List function returns the list store object.
func (s *DBSupplier) List() ListStore {
	return s.oldStores.listStore
}

// User function returns the user store object.
func (s *DBSupplier) User() UserStore {
	return s.oldStores.userStore
}

// Provider function returns the provider store object.
func (s *DBSupplier) Provider() ProviderStore {
	return s.oldStores.providerStore
}
