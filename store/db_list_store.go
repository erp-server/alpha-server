package store

import (
	"net/http"

	"gitlab.com/alpha/alpha-server/model"
)
// DBListStore struct
type DBListStore struct {
	DBStore
}
// NewDBListStore function create the instance of DBListStore struct.
func NewDBListStore(dbStore DBStore) ListStore {
	s := &DBListStore{dbStore}
	return s
}

// Read function get the item based on given query from given collection.
func (s *DBListStore) Read(collection string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if items, err := s.DBStore.Search(collection, query, 0, 0); err != nil {
			result.Err = model.NewAppError("DBItemStore.Read", "store.db_item.read.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = model.PropsFromBson(items)
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Delete function delete the item based on given query from given collection.
func (s *DBListStore) Delete(collection string, query interface{}) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if err := s.DBStore.DeleteDocument(collection, query.(string)); err != nil {
			result.Err = model.NewAppError("DBItemStore.Delete", "store.db_item.delete.app_error", nil, err.Error(), http.StatusBadRequest)
		} else {
			result.Data = &model.SuccessResponse{
				Success: true,
			}
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
