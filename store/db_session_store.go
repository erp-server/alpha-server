package store

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

// DBSessionStore struct
type DBSessionStore struct {
	DBStore
}

// NewDBSessionStore function create the instance of DBSessionStore
func NewDBSessionStore(dbStore DBStore) SessionsStore {
	ss := &DBSessionStore{dbStore}
	return ss
}

// Remove function delete the session from sessions collection.
func (s *DBSessionStore) Remove() StoreChannel {
	storeChannel := make(StoreChannel, 1)

	return storeChannel
}

// RemoveAllSession function delete all session from session collections.
func (s *DBSessionStore) RemoveAllSession() StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if dropped, err := s.DBStore.DropCollection(model.SESSIONS_TABLE); err != nil {
			result.Err = model.NewAppError("DBSessionStore.RemoveAllSession", "store.db_session.remove_all_session.app_error", nil, "", http.StatusBadRequest)
		} else {
			success := model.SuccessResponse{Success: dropped}
			result.Data = success
		}
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Create function create the new session in sessions collection.
func (s *DBSessionStore) Create(session *model.Session) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if len(session.Id) > 0 {
			result.Err = model.NewAppError("DBSessionStore.Create", "store.db_session.Create.existing.app_error", nil, "id="+session.Id.Hex(), http.StatusBadRequest)
			storeChannel <- result
			close(storeChannel)
			return
		}

		session.BeforeSave()
		if err := s.DBStore.CreateDocument(model.SESSIONS_TABLE, session); err != nil {
			result.Err = model.NewAppError("DBSessionStore.Create", "store.db_session.Create.error.app_error", nil, "id="+session.Id.Hex(), http.StatusNotAcceptable)
		}
		result.Data = session
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

// Get the session from give token.
// 
// token string token which used to get the session.
func (s *DBSessionStore) Get(token string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		if len(token) == 0 {
			result.Err = model.NewAppError("DBSessionStore.Get", "store.db_session.get.empty_token.app_error", nil, "Empty token", http.StatusBadRequest)
			storeChannel <- result
			close(storeChannel)
			return
		}

		sessionProps, err := s.DBStore.FindOne(model.SESSIONS_TABLE, bson.M{"token": token})
		if err != nil {
			result.Err = model.NewAppError("DBSessionStore.Get",
				"store.db_session.get.not_found.app_error",
				nil,
				"token"+token,
				http.StatusBadRequest)
		}
		var session *model.Session
		session = session.FromBSON(sessionProps)
		result.Data = session
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
