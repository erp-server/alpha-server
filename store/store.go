package store

import (
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
)

// StoreResult struct
type StoreResult struct {
	Data interface{}
	Err  *model.AppError
}

// StoreChannel type 
type StoreChannel chan StoreResult

// Store interface
type Store interface {
	Item() ItemStore
	List() ListStore
	User() UserStore
	Session() SessionsStore
	//Provider() ProviderStore
}

// ProviderStore interface
type ProviderStore interface {
	MessageStoreProvider() MsgStore
}

// MsgStore interface 
type MsgStore interface {
	createEntryId(*model.EntryId) string
	createStoreEntryId(*model.StoreEntryId) string
	createMessageStore(*model.User) StoreChannel
	createRootContainer(props *model.Props) StoreChannel
	getDefaultMessageStore(user *model.User) StoreChannel
	getPluginRootFolder(user *model.User) StoreChannel
	getByEntryId(id string) StoreChannel
	createIPMPluginsRoot(props *model.Props) StoreChannel
	createDefaultFolders(props *model.Props) StoreChannel
}

// ItemStore interface
type ItemStore interface {
	Create(collection string, props *model.Props) StoreChannel
	Update(collection string, query interface{}) StoreChannel
	Delete(collection string, query interface{}) StoreChannel
	Read(string, bson.ObjectId) StoreChannel
	ReadAll(collation string, query interface{}) StoreChannel
}

// ListStore interface
type ListStore interface {
	Read(collation string, query interface{}) StoreChannel
	Delete(collection string, query interface{}) StoreChannel
}

// SessionsStore interface
type SessionsStore interface {
	Create(*model.Session) StoreChannel
	Remove() StoreChannel
	RemoveAllSession() StoreChannel
	Get(string) StoreChannel
}

// UserStore interface
type UserStore interface {
	Create(user *model.User) StoreChannel
	GetForLogin(loginID string, query interface{}) StoreChannel
	Update(user *model.User) StoreChannel
	Read(query interface{}) StoreChannel
	Delete(ID string) StoreChannel
	GetUserByID(ID string) StoreChannel
	GetContexts(user *model.User) StoreChannel
}
