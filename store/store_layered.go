package store

import (
	"context"

	"gitlab.com/alpha/alpha-server/model"
)

// LayeredStore sturct
type LayeredStore struct {
	TmpContext    context.Context
	DatabaseLayer *DBSupplier
}

// NewLayeredStore function create the new instance of layered store.
func NewLayeredStore(config *model.DataBaseSettings) Store {
	store := &LayeredStore{
		TmpContext:    context.TODO(),
		DatabaseLayer: NewDBSupplier(config),
	}
	return store
}

// User function returns the UserStore.
func (s *LayeredStore) User() UserStore {
	return s.DatabaseLayer.User()
}

// Session function returns the SessionsStore.
func (s *LayeredStore) Session() SessionsStore {
	return s.DatabaseLayer.Session()
}

// Item function returns the ItemStore.
func (s *LayeredStore) Item() ItemStore {
	return s.DatabaseLayer.Item()
}

// List function returns the ListStore.
func (s *LayeredStore) List() ListStore {
	return s.DatabaseLayer.List()
}
