package api

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/alpha/alpha-server/model"
)

func (api *API) InitEMP() {
	api.BaseRoutes.EMP.Handle("", api.ApiSessionRequired(createEmployee)).Methods("POST")
	api.BaseRoutes.EMP.Handle("/list", api.ApiSessionRequired(listEmployees)).Methods("POST")
	api.BaseRoutes.EMP.Handle("/delete/{id:[A-Za-z0-9_-]+}", api.ApiSessionRequired(deleteEmployee)).Methods("POST")
	api.BaseRoutes.EMP.Handle("/{id:[A-Za-z0-9_-]+}/leaves", api.ApiSessionRequired(employeeLeaves)).Methods("POST")
	api.BaseRoutes.EMP.Handle("/{id:[A-Za-z0-9_-]+}", api.ApiSessionRequired(getEmployee)).Methods("POST")
}

func createEmployee(c *Context, w http.ResponseWriter, r *http.Request) {
	EMP := model.NewProps()
	employee := EMP.PropsFromJson(r.Body)
	if employee == nil {
		return
	}

	resultEmp, err := c.App.CreateEmployee(employee)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(resultEmp.ToJson()))
}

func listEmployees(c *Context, w http.ResponseWriter, r *http.Request) {
	resultEmp, err := c.App.ListEmployee()
	if err != nil {
		c.Err = err
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(model.PropsMapToJson(resultEmp)))
}

func deleteEmployee(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	empId := params["id"]
	result, err := c.App.DeleteEmployee(empId)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(model.PropsToJson(result)))
}

func employeeLeaves(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	result, err := c.App.DeleteEmployee(id)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(model.PropsToJson(result)))
}

func getEmployee(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	employee, err := c.App.GetEmployee(id)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(model.PropsToJson(employee)))
}
