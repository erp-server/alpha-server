package api

import (
	"github.com/gorilla/mux"

	"gitlab.com/alpha/alpha-server/app"
)

type Routes struct {
	Root    *mux.Router // ''
	ApiRoot *mux.Router // 'api/v1'

	User *mux.Router
	EMP  *mux.Router
	CRM  *mux.Router
}

type API struct {
	App        *app.App
	BaseRoutes *Routes
}

func Init(a *app.App, root *mux.Router) *API {
	api := &API{
		App:        a,
		BaseRoutes: &Routes{},
	}
	api.BaseRoutes.Root = root
	api.BaseRoutes.ApiRoot = root.PathPrefix("/api/v1").Subrouter()
	api.BaseRoutes.User = api.BaseRoutes.ApiRoot.PathPrefix("/user").Subrouter()
	api.BaseRoutes.EMP = api.BaseRoutes.ApiRoot.PathPrefix("/emp").Subrouter()
	api.BaseRoutes.CRM = api.BaseRoutes.ApiRoot.PathPrefix("/crm").Subrouter()

	api.InitUser()
	api.InitEMP()
	api.InitCRM()
	return api
}
