package api

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/alpha/alpha-server/model"
)

func (api *API) InitUser() {
	api.BaseRoutes.User.Handle("", api.ApiHandler(createUser)).Methods("POST")
	api.BaseRoutes.User.Handle("/login", api.ApiHandler(login)).Methods("POST")
	api.BaseRoutes.User.Handle("/delete/{id:[A-Za-z0-9_-]+}", api.ApiSessionRequired(deleteUser)).Methods("POST")
	api.BaseRoutes.User.Handle("/list", api.ApiSessionRequired(listUsers)).Methods("POST")
	api.BaseRoutes.User.Handle("/{id:[A-Za-z0-9_-]+}/context", api.ApiSessionRequired(getContexts)).Methods("POST")
	api.BaseRoutes.User.Handle("/{id:[A-Za-z0-9_-]+}", api.ApiSessionRequired(getUserDetails)).Methods("POST")
}

func createUser(c *Context, w http.ResponseWriter, r *http.Request) {
	user := model.UserFromJson(r.Body)
	if user == nil {
		return
	}
	ruser, err := c.App.CreateUser(user)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(ruser.ToJson()))
}

func deleteUser(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	uid := params["id"]
	result, err := c.App.DeleteUserById(uid)

	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(model.PropsToJson(result)))
}

func login(c *Context, w http.ResponseWriter, r *http.Request) {
	props := model.MapFromJson(r.Body)
	id := props["id"]
	loginId := props["username"]
	password := props["password"]
	token := props["token"]

	user, err := c.App.AuthenticateUser(id, loginId, password, token)
	if err != nil {
		c.Err = err
		return
	}

	session, err := c.App.DoLogin(w, r, user)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(session.ToJSON()))
}

func listUsers(c *Context, w http.ResponseWriter, r *http.Request) {
	users, err := c.App.GetUsers()
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(model.PropsToJson(users)))
}

func getContexts(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	uid := params["id"]

	users, err := c.App.GetContext(uid)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(model.PropsToJson(users)))
}

func getUserDetails(c *Context, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	uid := params["id"]

	user, err := c.App.GetUserByID(uid)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(model.PropsToJson(user)))
}
