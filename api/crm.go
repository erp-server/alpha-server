package api

import (
	"net/http"

	"gitlab.com/alpha/alpha-server/model"
)

func (api *API) InitCRM() {
	api.BaseRoutes.User.Handle("", api.ApiSessionRequired(createContact)).Methods("POST")
}

func createContact(c *Context, w http.ResponseWriter, r *http.Request) {
	contactItem := model.NewProps()
	contact := contactItem.PropsFromJson(r.Body)
	if contact == nil {
		return
	}

	resultEmp, err := c.App.CreateContact(contact)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(resultEmp.ToJson()))
}
