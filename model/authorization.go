package model

type Permission struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Role struct {
	Id          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Permissions []string `json:"permissions"`
}

var PERMISSION_CREATE *Permission
var PERMISSION_READ *Permission
var PERMISSION_DELETE *Permission
var PERMISSION_UPDATE *Permission
var PERMISSION_CREATE_USER *Permission
var PERMISSION_DELETE_USER *Permission
var PERMISSION_UPDATE_USER *Permission
var PERMISSION_READ_USER *Permission

var ROLE_MANAGER_USER *Role
var ROLE_EMPLOYEE_USER *Role
var ROLE_SYSTEM_ADMIN *Role
var ROLE_SYSTEM_OWNER *Role

var BuiltInRoles map[string]*Role

func InitializePermissions() {
	PERMISSION_CREATE = &Permission{
		"create_items",
		"authentication.permissions.create_items.name",
		"authentication.permissions.create_items.description",
	}
	PERMISSION_CREATE_USER = &Permission{
		"create_user",
		"authentication.permissions.create_user.name",
		"authentication.permissions.create_items.description",
	}
	PERMISSION_DELETE_USER = &Permission{
		"delete_user",
		"authentication.permissions.delete_user.name",
		"authentication.permissions.delete_user.description",
	}
	PERMISSION_UPDATE_USER = &Permission{
		"update_user",
		"authentication.permissions.update_user.name",
		"authentication.permissions.update_user.description",
	}
	PERMISSION_READ_USER = &Permission{
		"read_user",
		"authentication.permissions.read_user.name",
		"authentication.permissions.read_user.description",
	}
	PERMISSION_READ = &Permission{
		"read_items",
		"authentication.permissions.read_items.name",
		"authentication.permissions.read_items.description",
	}
	PERMISSION_DELETE = &Permission{
		"delete_items",
		"authentication.permissions.delete_items.name",
		"authentication.permissions.delete_items.description",
	}
	PERMISSION_UPDATE = &Permission{
		"update_items",
		"authentication.permissions.update_items.name",
		"authentication.permissions.update_items.description",
	}
}

func InitializeRoles() {
	InitializePermissions()
	BuiltInRoles = make(map[string]*Role)

	ROLE_MANAGER_USER = &Role{
		"manager_user",
		"authentication.roles.manager_user.name",
		"authentication.roles.manager_user.description",
		[]string{
			PERMISSION_CREATE.Id,
			PERMISSION_READ.Id,
			PERMISSION_DELETE.Id,
			PERMISSION_UPDATE.Id,
		},
	}
	BuiltInRoles[ROLE_MANAGER_USER.Id] = ROLE_MANAGER_USER

	ROLE_EMPLOYEE_USER = &Role{
		"employee_user",
		"authentication.roles.employee_user.name",
		"authentication.roles.employee_user.description",
		[]string{
			PERMISSION_CREATE.Id,
			PERMISSION_READ.Id,
			PERMISSION_DELETE.Id,
			PERMISSION_UPDATE.Id,
		},
	}
	BuiltInRoles[ROLE_EMPLOYEE_USER.Id] = ROLE_EMPLOYEE_USER

	ROLE_SYSTEM_ADMIN = &Role{
		"admin_user",
		"authentication.roles.admin_user.name",
		"authentication.roles.admin_user.description",
		[]string{
			PERMISSION_CREATE_USER.Id,
			PERMISSION_DELETE_USER.Id,
			PERMISSION_UPDATE_USER.Id,
			PERMISSION_READ_USER.Id,
		},
	}
	BuiltInRoles[ROLE_SYSTEM_ADMIN.Id] = ROLE_SYSTEM_ADMIN

	ROLE_SYSTEM_OWNER = &Role{
		"owner_user",
		"authentication.roles.owner_user.name",
		"authentication.roles.owner_user.description",
		append(
			append([]string{}, ROLE_SYSTEM_ADMIN.Permissions...),
			ROLE_MANAGER_USER.Permissions...,
		),
	}
	BuiltInRoles[ROLE_SYSTEM_OWNER.Id] = ROLE_SYSTEM_OWNER
}

func init() {
	InitializeRoles()
}
