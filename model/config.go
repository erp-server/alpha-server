package model

import (
	"encoding/json"
	"os"
)

// const
const (
	SITE_URL = "http://localhost:8000"
)

// ServiceSettings strcut
type ServiceSettings struct {
	SiteURL                *string
	ListenAddress          *string
	DeployOnHeroku         *bool
	SessionLengthWebInDays *int64
}

// DataSource struct
type DataSource struct {
	ReplicaSets *[]string
	UserName    *string
	Password    *string
	Source      *string
}

// DataBaseSettings strcut
type DataBaseSettings struct {
	DriverName      *string
	DatabaseName    *string
	DataSourceAtlas *bool
	DataSource      DataSource
}

// Config strcut
type Config struct {
	ServiceSettings  ServiceSettings
	DataBaseSettings DataBaseSettings
}

// ToJSON function convert the session object to json.
func (o *Config) ToJSON() string {
	b, err := json.Marshal(o)
	if err != nil {
		return ""
	}
	return string(b)
}

// IsValid function check the session is valid or not.
func (o *Config) IsValid() *AppError {
	return nil
}

// SetDefaults function set the default values of configuration
func (o *Config) SetDefaults() {
	if o.ServiceSettings.SiteURL == nil {
		o.ServiceSettings.SiteURL = new(string)
		*o.ServiceSettings.SiteURL = SITE_URL
	}

	if o.ServiceSettings.ListenAddress == nil || *o.ServiceSettings.DeployOnHeroku == true {
		o.ServiceSettings.ListenAddress = new(string)
		*o.ServiceSettings.ListenAddress = ":" + os.Getenv("PORT")
	}

	if o.ServiceSettings.SessionLengthWebInDays == nil {
		o.ServiceSettings.SessionLengthWebInDays = new(int64)
		*o.ServiceSettings.SessionLengthWebInDays = int64(2)
	}
}
