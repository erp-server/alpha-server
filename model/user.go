package model

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id                       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	PR_CREATE_TIME           int64         `json:"create_time,omitempty" bson:"create_time,omitempty"`
	PR_LASTMODIFICATION_TIME int64         `json:"modified_time,omitempty" bson:"modified_time,omitempty"`
	PR_DELETE_AT             int64         `json:"delete_at,omitempty" bson:"delete_at"`
	PR_USERNAME              string        `json:"username,omitempty" bson:"username"`
	PR_PASSWORD              string        `json:"password,omitempty" bson:"password,omitempty"`
	PR_EMAIL_ADDRESS         string        `json:"email,omitempty" bson:"email"`
	PR_NICKNAME              string        `json:"nickname,omitempty" bson:"nickname"`
	PR_DISPLAYNAME           string        `json:"display_name,omitempty" bson:"display_name"`
	PR_FIRSTNAME             string        `json:"first_name,omitempty" bson:"first_name"`
	PR_LASTNAME              string        `json:"last_name,omitempty" bson:"last_name"`
	PR_POSITION              string        `json:"position,omitempty" bson:"position"`
	PR_ROLES                 string        `json:"roles,omitempty" bson:"roles"`
	PR_LASTMODIFIED_PASSWORD int64         `json:"last_modified_password,omitempty" bson:"last_modified_password,omitempty"`
	PR_LOGIN_ATTEMPTS        int64         `json:"login_attempts,omitempty" bson:"login_attempts"`
	PR_LOCALE                string        `json:"locale,omitempty" bson:"locale"`
}

func (u *User) IsValid() bool {

	return true
}

// ToJson convert a User to a json string
func (u *User) ToJson() string {
	b, err := json.Marshal(u)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

// UserFromJson will decode the input and return a User
func UserFromJson(data io.Reader) *User {
	decoder := json.NewDecoder(data)
	var user User
	err := decoder.Decode(&user)
	if err == nil {
		return &user
	} else {
		return nil
	}
}

// UsersFromBson will decode the input and return a User
func UsersFromBson(data []bson.M) []*User {
	var users []*User
	for _, v := range data {
		var user User
		b, _ := bson.Marshal(v)
		bson.Unmarshal(b, &user)
		users = append(users, &user)
	}
	return users
}

// UserFromBSON will decode the input and return a User
func UserFromBSON(data bson.M) *User {
	var user *User
	uProps, _ := bson.Marshal(data)
	bson.Unmarshal(uProps, &user)
	return user
}

// ComparePassword compares the hash
func ComparePassword(hash string, password string) bool {
	if len(password) == 0 || len(hash) == 0 {
		return false
	}

	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// HashPassword generates a hash using the bcrypt.GenerateFromPassword
func HashPassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		panic(err)
	}

	return string(hash)
}

func (u *User) PreSave() {
	if u.Id == "" {
		u.Id = bson.NewObjectId()
	}

	if u.PR_USERNAME == "" {
		u.PR_USERNAME = NewId()
	}

	u.PR_USERNAME = strings.ToLower(u.PR_USERNAME)
	u.PR_EMAIL_ADDRESS = strings.ToLower(u.PR_EMAIL_ADDRESS)

	u.PR_DISPLAYNAME = fmt.Sprintf("%s %s", u.PR_FIRSTNAME, u.PR_LASTNAME)
	u.PR_CREATE_TIME = GetMillis()
	u.PR_LASTMODIFICATION_TIME = u.PR_CREATE_TIME

	u.PR_LASTMODIFIED_PASSWORD = u.PR_CREATE_TIME

	if u.PR_LOCALE == "" {
		u.PR_LOCALE = "en"
	}

	if len(u.PR_PASSWORD) > 0 {
		u.PR_PASSWORD = HashPassword(u.PR_PASSWORD)
	}
}

func (u *User) Sensitized() {
	u.PR_CREATE_TIME = 0
	u.PR_DELETE_AT = 0
	u.PR_LASTMODIFICATION_TIME = 0
	u.PR_LASTMODIFIED_PASSWORD = 0
	u.PR_LOGIN_ATTEMPTS = 0
	u.PR_PASSWORD = ""
}
