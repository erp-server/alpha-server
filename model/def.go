package model

import (
	"encoding/json"
	"io"

	"gopkg.in/mgo.v2/bson"
)

const (
	MAPI_STORE               = 0x00000001 /* Message Store */
	MAPI_ADDRBOOK            = 0x00000002 /* Address Book */
	MAPI_FOLDER              = 0x00000003 /* Folder */
	MAPI_ABCONT              = 0x00000004 /* Address Book Container */
	MAPI_MESSAGE             = 0x00000005 /* Message */
	MAPI_MAILUSER            = 0x00000006 /* Individual Recipient */
	MAPI_ATTACH              = 0x00000007 /* Attachment */
	MAPI_DISTLIST            = 0x00000008 /* Distribution List*/
	TABLE_SORT_ASCEND        = 0x00000000
	TABLE_SORT_DESCEND       = 0x00000001
	TABLE_SORT_COMBINE       = 0x00000002
	ERP_SERVICE_GUID         = "87d7e997a92144bb8c535df3b4bfe5f3" // default store
	ERP_STORE_PUBLIC_GUID    = "2fa9730538574463b1756806b3782d35"
	ERP_STORE_DELEGATE_GUID  = "2fa9730538574463b1756806b3782d36"
	ERP_STORE_ARCHIVER_GUID  = "2fa9730538574463b1756806b3782d37"
	OBJECTS_TABLE            = "objects"
	SESSIONS_TABLE           = "sessions"
	DB_USER_TABLE            = "users"
	ERP_PRIVATE_FOLDER       = 0X001
	ERP_PUBLIC_FOLDER        = 0X003
	ERP_MAPPEDPUBLIC_FOLDER  = 0X005
	ERP_PRIVATE_MESSAGE      = 0X007
	ERP_PUBLIC_MESSAGE       = 0X009
	ERP_MAPPEDPUBLIC_MESSAGE = 0X00B
	FOLDER_ROOT              = 0x00000000
	FOLDER_GENERIC           = 0x00000001
	FOLDER_SEARCH            = 0x00000002
)

// Fixme :
type Props struct {
	Id                            bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
	PR_ENTRYID                    string        `json:"entryid,omitempty" bson:"entryid,omitempty"`
	PR_PARENT_ENTRYID             string        `json:"parent_entryid,omitempty" bson:"parent_entryid,omitempty"`
	PR_STORE_ENTRYID              string        `json:"store_entryid,omitempty" bson:"store_entryid,omitempty"`
	PR_STORE_RECORD_KEY           string        `json:"store_record_key,omitempty" bson:"store_record_key,omitempty"`
	PR_RECORD_KEY                 string        `json:"record_key,omitempty" bson:"record_key,omitempty"`
	PR_DISPLAY_NAME               string        `json:"display_name,omitempty" bson:"display_name,omitempty"`
	PR_DEFAULT_STORE              bool          `json:"default_store,omitempty" bson:"default_store,omitempty"`
	PR_STORE_SUPPORT_MASK         int64         `json:"store_support_mask,omitempty" bson:"store_support_mask,omitempty"`
	PR_STORE_STATE                int64         `json:"store_state,omitempty" bson:"store_state,omitempty"`
	PR_IPM_PLUGIN_ROOT_SEARCH_KEY string        `json:"ipm_subtree_search_key,omitempty" bson:"ipm_subtree_search_key ,omitempty"`
	PR_IPM_WASTEBASKET_SEARCH_KEY string        `json:"ipm_wastebasket_search_key,omitempty" bson:"ipm_wastebasket_search_key,omitempty"`
	PR_MDB_PROVIDER               string        `json:"mdb_provider,omitempty" bson:"mdb_provider,omitempty"`
	PR_RECEIVE_FOLDER_SETTINGS    string        `json:"receive_folder_settings,omitempty" bson:"receive_folder_settings,omitempty"`
	PR_VALID_FOLDER_MASK          string        `json:"valid_folder_mask ,omitempty" bson:"valid_folder_mask,omitempty"`
	PR_IPM_PLUGINS_ROOT_ENTRYID   string        `json:"ipm_plugin_root_entryid,omitempty" bson:"ipm_plugin_root_entryid,omitempty"`
	PR_IPM_WASTEBASKET_ENTRYID    string        `json:"ipm_wastebasket_entryid,omitempty" bson:"ipm_wastebasket_entryid,omitempty"`
	PR_IPM_CRM_ENTRYID            string        `json:"ipm_crm_entryid,omitempty" bson:"ipm_crm_entryid,omitempty"`
	PR_VIEWS_ENTRYID              string        `json:"views_entryid,omitempty" bson:"views_entryid,omitempty"`
	PR_COMMON_VIEWS_ENTRYID       string        `json:"common_views_entryid,omitempty" bson:"common_views_entryid,omitempty"`
	PR_FINDER_ENTRYID             string        `json:"finder_entryid,omitempty" bson:"finder_entryid,omitempty"`
	PR_EC_ERP_SETTINGS_JSON       string        `json:"ec_erp_settings_json,omitempty" bson:"ec_erp_settings_json,omitempty"`
	PR_OBJECT_TYPE                int64         `json:"object_type,omitempty" bson:"object_type,omitempty"`
	PR_FOLDER_TYPE                int64         `json:"folder_type,omitempty" bson:"folder_type,omitempty"`
	PR_MAPPING_SIGNATURE          string        `json:"mapping_signature,omitempty" bson:"mapping_signature,omitempty"`
	PR_SUBFOLDERS                 bool          `json:"subfolders,omitempty" bson:"subfolders,omitempty"`
	PR_ACCESS                     int64         `json:"access,omitempty" bson:"access,omitempty"`
	PR_SUBJECT                    string        `json:"subject,omitempty" bson:"subject,omitempty"`
	PR_BODY                       string        `json:"body,omitempty" bson:"body,omitempty"`
	PR_ICON_INDEX                 int64         `json:"icon_index,omitempty" bson:"icon_index,omitempty"`
	PR_MESSAGE_CLASS              string        `json:"message_class,omitempty" bson:"message_class,omitempty"`
	PR_MESSAGE_FLAGS              string        `json:"message_flags,omitempty" bson:"message_flags,omitempty"`
	PR_MESSAGE_SIZE               int64         `json:"message_size,omitempty" bson:"message_size,omitempty"`
	PR_MIDDLE_NAME                string        `json:"middle_name,omitempty" bson:"middle_name,omitempty"`
	PR_SURNAME                    string        `json:"surname,omitempty" bson:"surname,omitempty"`
	PR_HOME_TELEPHONE_NUMBER      int64         `json:"home_telephone_number,omitempty" bson:"home_telephone_number,omitempty"`
	PR_CELLULAR_TELEPHONE_NUMBER  int64         `json:"cellular_telephone_number,omitempty" bson:"cellular_telephone_number,omitempty"`
	PR_BUSINESS_TELEPHONE_NUMBER  int64         `json:"business_telephone_number,omitempty" bson:"business_telephone_number,omitempty"`
	PR_BUSINESS_FAX_NUMBER        int64         `json:"business_fax_number,omitempty" bson:"business_fax_number,omitempty"`
	PR_COMPANY_NAME               string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	PR_DEPARTMENT_NAME            string        `json:"department_name,omitempty" bson:"department_name,omitempty"`
	PR_OFFICE_LOCATION            string        `json:"office_location,omitempty" bson:"office_location,omitempty"`
	PR_PROFESSION                 string        `json:"profession,omitempty" bson:"profession,omitempty"`
	PR_MANAGER_NAME               string        `json:"manager_name,omitempty" bson:"manager_name,omitempty"`
	PR_ASSISTANT                  string        `json:"assistant,omitempty" bson:"assistant,omitempty"`
	PR_NICKNAME                   string        `json:"nickname,omitempty" bson:"nickname,omitempty"`
	PR_DISPLAY_NAME_PREFIX        string        `json:"display_name_prefix,omitempty" mapstructure:"display_name_prefix" bson:"display_name_prefix,omitempty"`
	PR_SPOUSE_NAME                string        `json:"spouse_name,omitempty" bson:"spouse_name,omitempty"`
	PR_GENERATION                 string        `json:"generation,omitempty" bson:"generation,omitempty"`
	PR_BIRTHDAY                   string        `json:"birthday,omitempty" bson:"birthday,omitempty"`
	PR_WEDDING_ANNIVERSARY        string        `json:"wedding_anniversary,omitempty" bson:"wedding_anniversary,omitempty"`
	PR_SENSITIVITY                string        `json:"sensitivity,omitempty" bson:"sensitivity,omitempty"`
	PR_HASATTACH                  bool          `json:"hasattach,omitempty" bson:"hasattach,omitempty"`
	PR_COUNTRY                    string        `json:"country,omitempty" bson:"country,omitempty"`
	PR_LOCALITY                   string        `json:"locality,omitempty" bson:"locality,omitempty" `
	PR_POSTAL_ADDRESS             string        `json:"postal_address,omitempty" bson:"postal_address,omitempty"`
	PR_POSTAL_CODE                int64         `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	PR_STATE_OR_PROVINCE          string        `json:"state_or_province,omitempty" bson:"state_or_province,omitempty"`
	PR_STREET_ADDRESS             string        `json:"street_address,omitempty" bson:"street_address,omitempty"`

}

func NewProps() *Props {
	return &Props{}
}

func (u *Props) ToJson() string {
	b, err := json.Marshal(u)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func PropsToJson(props interface{}) string {
	b, err := json.Marshal(props)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func PropsMapToJson(u []*Props) string {
	b, err := json.Marshal(u)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func (u *Props) PropsFromJson(data io.Reader) *Props {
	decoder := json.NewDecoder(data)
	props := NewProps()
	err := decoder.Decode(props)
	if err == nil {
		return props
	} else {
		return nil
	}
}

func PropsFromBson(data []bson.M) []*Props {
	var props []*Props
	for _, v := range data {
		prop := NewProps()
		b, _ := bson.Marshal(v)
		bson.Unmarshal(b, prop)
		props = append(props, prop)
	}
	return props
}

func PropFromBson(data bson.M) *Props {
	var props *Props
	uProps, _ := bson.Marshal(data)
	bson.Unmarshal(uProps, &props)
	return props
}

type SuccessResponse struct {
	Success bool `json:"success"`
}
