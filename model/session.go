package model

import (
	"encoding/json"

	"gopkg.in/mgo.v2/bson"
)

const (
	SESSION_COOKIE_TOKEN              = "AUTHTOKEN"
	SESSION_COOKIE_USER               = "USERID"
	SESSION_CACHE_SIZE                = 35000
	SESSION_PROP_BROWSER              = "browser"
	SESSION_PROP_TYPE                 = "type"
	SESSION_PROP_USER_ACCESS_TOKEN_ID = "user_access_token_id"
	SESSION_TYPE_USER_ACCESS_TOKEN    = "UserAccessToken"
	SESSION_ACTIVITY_TIMEOUT          = 1000 * 60 * 5 // 5 minutes
	SESSION_USER_ACCESS_TOKEN_EXPIRY  = 100 * 365     // 100 years
)

type Session struct {
	Id                    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	PR_TOKEN              string        `json:"token" bson:"token"`
	PR_CRIATION_TIME      int64         `json:"create_at" bson:"create_at"`
	PR_EXPIRES_TIME       int64         `json:"expires_at" bson:"expires_at"`
	PR_LAST_ACTIVITY_TIME int64         `json:"last_activity_at" bson:"last_activity_at"`
	PR_USER_ID            string        `json:"user_id" bson:"user_id"`
	PR_ROLES              string        `json:"roles" bson:"roles"`
	PR_IS_OAUTH           bool          `json:"is_oauth" bson:"is_oauth"`
}

// DeepCopy function used to create copy of session object.
func (me *Session) DeepCopy() *Session {
	copy := *me
	return &copy
}

// ToJSON function convert the session object to JSON formate.
func (me *Session) ToJSON() string {
	b, err := json.Marshal(me)
	if err != nil {
		return ""
	}
	return string(b)
}

// FromBSON will decode the input and return a User
func (me *Session) FromBSON(data bson.M) *Session {
	var session *Session
	uProps, _ := bson.Marshal(data)
	bson.Unmarshal(uProps, &session)
	return session
}

// IsExpired function return true if session is exipired else false.
func (me *Session) IsExpired() bool {

	if me.PR_EXPIRES_TIME <= 0 {
		return false
	}

	if GetMillis() > me.PR_EXPIRES_TIME {
		return true
	}

	return false
}

// SetExpireInDays function set the session expire in number of days.
func (me *Session) SetExpireInDays(days int64) {
	if me.PR_CRIATION_TIME == 0 {
		me.PR_EXPIRES_TIME = GetMillis() + int64(1000*60*60*24*days)
	} else {
		me.PR_EXPIRES_TIME = me.PR_CRIATION_TIME + int64(1000*60*60*24*days)
	}
}

// BeforeSave function called before the session gets saved.
func (me *Session) BeforeSave() {
	if me.Id == "" {
		me.Id = bson.NewObjectId()
	}

	if me.PR_TOKEN == "" {
		me.PR_TOKEN = NewId()
	}

	me.PR_CRIATION_TIME = GetMillis()
	me.PR_LAST_ACTIVITY_TIME = me.PR_CRIATION_TIME
}
