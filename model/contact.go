package model

import 	"gopkg.in/mgo.v2/bson"

type Contact struct {
	Id                       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	PR_CREATE_TIME           int64         `json:"create_time,omitempty" bson:"create_time,omitempty"`
	PR_LASTMODIFICATION_TIME int64         `json:"modified_time,omitempty" bson:"modified_time,omitempty"`
	PR_DELETE_AT             int64         `json:"delete_at,omitempty" bson:"delete_at"`
	PR_USERNAME              string        `json:"username,omitempty" bson:"username"`
	PR_PASSWORD              string        `json:"password,omitempty" bson:"password,omitempty"`
	PR_EMAIL_ADDRESS         string        `json:"email,omitempty" bson:"email"`
	PR_NICKNAME              string        `json:"nickname,omitempty" bson:"nickname"`
	PR_DISPLAYNAME           string        `json:"display_name,omitempty" bson:"display_name"`
	PR_FIRSTNAME             string        `json:"first_name,omitempty" bson:"first_name"`
	PR_LASTNAME              string        `json:"last_name,omitempty" bson:"last_name"`
	PR_POSITION              string        `json:"position,omitempty" bson:"position"`
	PR_ROLES                 string        `json:"roles,omitempty" bson:"roles"`
	PR_LASTMODIFIED_PASSWORD int64         `json:"last_modified_password,omitempty" bson:"last_modified_password,omitempty"`
	PR_LOGIN_ATTEMPTS        int64         `json:"login_attempts,omitempty" bson:"login_attempts"`
	PR_LOCALE                string        `json:"locale,omitempty" bson:"locale"`
}
