package model

import (
	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

type StoreEntryId struct {
	Flags              string
	ProviderGUID       string
	Version            string
	Flag               string
	WrappedFlags       string
	WrappedProviderUID string
	WrappedType        string
}

type EntryId struct {
	Flags        string
	ProviderGUID string
	FolderType   string
	DatabaseGUID string
	GlobeCounter string
	Pad          string
	Version      int
}

func (i *EntryId) PreSave() {
	if len(i.Flags) == 0 {
		i.Flags = "00000"
	}

	if len(i.FolderType) == 0 {
		i.FolderType = strconv.Itoa(ERP_PRIVATE_FOLDER)
	}

	if len(i.GlobeCounter) == 0 {
		i.GlobeCounter = strconv.FormatInt(GetMillis(), 16)
	}
	if len(i.Pad) == 0 {
		i.Pad = "0000"
	}
}

func (s *StoreEntryId) PreSave() {
	if len(s.Flags) == 0 {
		s.Flags = "00000000"
	}

	if len(s.ProviderGUID) == 0 {
		s.ProviderGUID = "38a1bb1005e5101aa1bb08002b2a56c2"
	}

	if len(s.Version) == 0 {
		s.Version = "00"
	}

	if len(s.Flags) == 0 {
		s.Flag = "00"
	}

	if len(s.WrappedFlags) == 0 {
		s.WrappedFlags = "00000000"
	}

	s.WrappedType = "0c000000"
}

func (i *EntryId) IsValid() *AppError {
	if len(i.Flags) == 0 {
		return InvalidEntryIdError("entryid_data_flags")
	}

	if len(i.FolderType) == 0 {
		return InvalidEntryIdError("entryid_data_foldertype")
	}

	if !bson.IsObjectIdHex(i.ProviderGUID) {
		return InvalidEntryIdError("entryid_data_providerguid")
	}

	if !bson.IsObjectIdHex(i.DatabaseGUID) {
		return InvalidEntryIdError("entryid_data_databaseguid")
	}

	return nil
}

func InvalidEntryIdError(fieldName string) *AppError {
	id := fmt.Sprintf("model.message_store_provider.is_valid.%s.app_error", fieldName)
	return NewAppError("EntryId.IsValid", id, nil, "", http.StatusBadRequest)
}
