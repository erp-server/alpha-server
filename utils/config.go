package utils

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"

	"github.com/spf13/viper"

	"gitlab.com/alpha/alpha-server/model"
)

var cfgMutex = &sync.Mutex{}
var Cfg = &model.Config{}
var CfgDiagnosticId = ""
var CfgHash = ""
var ClientCfgHash = ""
var CfgFileName string = ""
var CfgDisableConfigWatch = false

func LoadConfig(fileName string) (*model.Config, string, *model.AppError) {
	cfgMutex.Lock()
	defer cfgMutex.Unlock()

	fileNameWithExtension := filepath.Base(fileName)
	fileExtension := filepath.Ext(fileNameWithExtension)
	fileDir := filepath.Dir(fileName)

	if len(fileNameWithExtension) > 0 {
		fileNameOnly := fileNameWithExtension[:len(fileNameWithExtension)-len(fileExtension)]
		viper.SetConfigName(fileNameOnly)
	} else {
		viper.SetConfigName("config")
	}

	if len(fileDir) > 0 {
		viper.AddConfigPath(fileDir)
	}

	viper.SetConfigType("json")
	viper.AddConfigPath("./config")
	viper.AddConfigPath("../config")
	viper.AddConfigPath("../../config")
	viper.AddConfigPath(".")

	configReadErr := viper.ReadInConfig()
	if configReadErr != nil {
		if _, ok := configReadErr.(viper.ConfigFileNotFoundError); ok {
			// In case of a file-not-found error, try to copy default.json if it's present.
			defaultPath := FindConfigFile("config.json")
			if src, err := os.Open(defaultPath); err == nil {
				if dest, err := os.OpenFile(filepath.Join(filepath.Dir(defaultPath), "config.json"), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600); err == nil {
					if _, err := io.Copy(dest, src); err == nil {
						configReadErr = viper.ReadInConfig()
					}
					dest.Close()
				}
				src.Close()
			}
		}
	}

	if configReadErr != nil {
		errMsg := T("utils.config.load_config.opening.panic", map[string]interface{}{"Filename": fileName})
		fmt.Fprintln(os.Stderr, errMsg)
		os.Exit(1)
	}

	var config model.Config
	unmarshalErr := viper.Unmarshal(&config)
	if unmarshalErr != nil {
		errMsg := T("utils.config.load_config.decoding.panic", map[string]interface{}{"Filename": fileName})
		fmt.Fprintln(os.Stderr, errMsg)
		os.Exit(1)
	}

	CfgFileName = viper.ConfigFileUsed()
	Cfg = &config
	CfgHash = fmt.Sprintf("%x", md5.Sum([]byte(Cfg.ToJSON())))
	return Cfg, fileDir, nil
}

func FindConfigFile(fileName string) string {
	if _, err := os.Stat("./config/" + fileName); err == nil {
		fileName, _ = filepath.Abs("./config/" + fileName)
	} else if _, err := os.Stat("../config/" + fileName); err == nil {
		fileName, _ = filepath.Abs("../config/" + fileName)
	} else if _, err := os.Stat(fileName); err == nil {
		fileName, _ = filepath.Abs(fileName)
	}

	return fileName
}
