package utils

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/nicksnyder/go-i18n/i18n"
)

var T i18n.TranslateFunc
var TDefault i18n.TranslateFunc
var locales = make(map[string]string)

func TranslationsPreInit() error {
	if err := InitTranslationsWithDir("i18n"); err != nil {
		log.Println(err)
		return err
	}

	T = TfuncWithFallback("en")
	TDefault = TfuncWithFallback("en")

	return nil
}

func GetTranslationsAndLocale(w http.ResponseWriter, r *http.Request) (i18n.TranslateFunc, error) {
	return TfuncWithFallback("en"), nil
}

func InitTranslationsWithDir(dir string) error {
	i18nDirectory, found := FindDir(dir)
	if !found {
		return fmt.Errorf("Unable to find i18n directory")
	}

	files, _ := ioutil.ReadDir(i18nDirectory)
	for _, f := range files {
		if filepath.Ext(f.Name()) == ".json" {
			filename := f.Name()
			locales[strings.Split(filename, ".")[0]] = i18nDirectory + filename

			if err := i18n.LoadTranslationFile(i18nDirectory + filename); err != nil {
				return err
			}
		}
	}

	return nil
}

func TfuncWithFallback(pref string) i18n.TranslateFunc {
	t, _ := i18n.Tfunc(pref)
	return func(translationID string, args ...interface{}) string {
		if translated := t(translationID, args...); translated != translationID {
			return translated
		}

		t, _ := i18n.Tfunc("en")
		return t(translationID, args...)
	}
}

func FindDir(dir string) (string, bool) {
	fileName := "."
	found := false

	if _, err := os.Stat("./" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("./" + dir + "/")
		found = true
	} else if _, err := os.Stat("../" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("../" + dir + "/")
		found = true
	} else if _, err := os.Stat("../../" + dir + "/"); err == nil {
		fileName, _ = filepath.Abs("../../" + dir + "/")
		found = true
	}

	return fileName + "/", found
}

func GetIpAddress(r *http.Request) string {
	return r.RemoteAddr
}

func GetSupportedLocales() map[string]string {
	return locales
}
