package app

import (
	"net/http"
	"time"

	"gitlab.com/alpha/alpha-server/model"
)

// AuthenticateUser function authenticat the user with backend
func (a *App) AuthenticateUser(ID, loginID, password, token string) (*model.User, *model.AppError) {
	if len(password) == 0 {
		err := model.NewAppError("AuthenticateUser", "api.user.login.blank_pwd.app_error", nil, "", http.StatusBadRequest)
		return nil, err
	}
	var user *model.User
	var err *model.AppError

	if len(ID) != 0 {
		// get user using userid.
	} else {
		if user, err = a.GetUserFromLoginID(loginID); err != nil {
			err = model.NewAppError("AuthenticateUser", "api.user.login.get_user.app_error", nil, "", http.StatusBadRequest)
			return nil, err
		} else if user, err = a.authenticateUser(user, password, token); err != nil {
			// and then authenticate them
			return nil, err
		}
	}

	return user, err
}

// DoLogin function perform the login
func (a *App) DoLogin(w http.ResponseWriter, r *http.Request, user *model.User) (*model.Session, *model.AppError) {
	session := &model.Session{PR_USER_ID: user.Id.Hex(), PR_IS_OAUTH: true}
	session.SetExpireInDays(*a.Config().ServiceSettings.SessionLengthWebInDays)

	maxAge := session.PR_EXPIRES_TIME
	var err *model.AppError
	if session, err = a.CreateSession(session); err != nil {
		err.StatusCode = http.StatusInternalServerError
		return nil, err
	}
	w.Header().Set(model.HEADER_TOKEN, session.PR_TOKEN)

	secure := false
	if GetProtocol(r) == "https" {
		secure = true
	}
	expiresAt := time.Unix(model.GetMillis()/1000+maxAge, 0)
	sessionCookie := &http.Cookie{
		Name:     model.SESSION_COOKIE_TOKEN,
		Value:    session.PR_TOKEN,
		Path:     "/",
		MaxAge:   int(maxAge),
		Expires:  expiresAt,
		Domain:   "localhost",
		HttpOnly: true,
		Secure:   secure,
	}

	userCookie := &http.Cookie{
		Name:    model.SESSION_COOKIE_USER,
		Value:   user.Id.Hex(),
		Path:    "/",
		MaxAge:  int(maxAge),
		Expires: expiresAt,
		Domain:  "localhost",
		Secure:  secure,
	}

	http.SetCookie(w, sessionCookie)
	http.SetCookie(w, userCookie)

	return session, nil
}

// GetProtocol return the protocol
func GetProtocol(r *http.Request) string {
	if r.Header.Get(model.HEADER_FORWARDED_PROTO) == "https" || r.TLS != nil {
		return "https"
	}
	return "http"
}
