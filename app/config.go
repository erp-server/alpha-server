package app

import (
	"strings"

	"gitlab.com/alpha/alpha-server/model"
	"gitlab.com/alpha/alpha-server/utils"
)

func (a *App) Config() *model.Config {
	if cfg := a.config.Load(); cfg != nil {
		return cfg.(*model.Config)
	}
	return &model.Config{}
}

func (a *App) LoadConfig(configFile string) *model.AppError {
	cfg, configPath, err := utils.LoadConfig(configFile)
	if err != nil {
		return err
	}
	*cfg.ServiceSettings.SiteURL = strings.TrimRight(*cfg.ServiceSettings.SiteURL, "/")
	a.config.Store(cfg)

	a.configFile = configPath
	return nil
}
