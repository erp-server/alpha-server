package app

import (
	"log"
	"net/http"

	l4g "github.com/alecthomas/log4go"

	"gitlab.com/alpha/alpha-server/model"
	"gitlab.com/alpha/alpha-server/utils"
)

func (a *App) CreateUser(user *model.User) (*model.User, *model.AppError) {
	user.PR_ROLES = model.ROLE_EMPLOYEE_USER.Id
	if user := <-a.Srv.Store.User().Create(user); user.Err != nil {
		l4g.Error(utils.T("api.user.create_user.save.error"), user.Err)
		return nil, user.Err
	} else {
		return user.Data.(*model.User), nil
	}
}

func (a *App) DeleteUserById(id string) (*model.SuccessResponse, *model.AppError) {
	if result := <-a.Srv.Store.User().Delete(id); result.Err != nil {
		l4g.Error(utils.T("api.user.delete_use.error"), result.Err)
		return nil, result.Err
	} else {
		return result.Data.(*model.SuccessResponse), nil
	}
}

// GetUserFromLoginID get the user from username
func (a *App) GetUserFromLoginID(userName string) (*model.User, *model.AppError) {
	if result := <-a.Srv.Store.User().GetForLogin(userName, nil); result.Err != nil {
		l4g.Error(utils.T("api.user.get_user_from_loginid.get.error"), result.Err)
		result.Err.StatusCode = http.StatusBadRequest
		return nil, result.Err
	} else {
		return result.Data.(*model.User), nil
	}
}

// GetUsers return array of the users
func (a *App) GetUsers() ([]*model.User, *model.AppError) {
	if result := <-a.Srv.Store.User().Read(nil); result.Err != nil {
		return nil, result.Err
	} else {
		return result.Data.([]*model.User), nil
	}
}

// GetUserByID find the user by user id.
func (a *App) GetUserByID(ID string) (*model.User, *model.AppError) {
	// get user from user id
	if result := <-a.Srv.Store.User().GetUserByID(ID); result.Err != nil {
		return nil, result.Err
	} else {
		// get plugin root folder from user info.
		return result.Data.(*model.User), nil
	}
}

// GetContext return contexts information of a selected user.
func (a *App) GetContext(id string) ([]*model.Props, *model.AppError) {
	// get user from user id
	if result := <-a.Srv.Store.User().GetUserByID(id); result.Err != nil {
		return nil, result.Err
	} else {
		log.Println("called")
		// get plugin root folder from user info.
		user := result.Data.(*model.User)
		log.Println("user:-", user)

		if result := <-a.Srv.Store.User().GetContexts(user); result.Err != nil {
			return nil, result.Err
		} else {
			return result.Data.([]*model.Props), nil
		}
	}
}
