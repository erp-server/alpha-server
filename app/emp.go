package app

import (
	"github.com/alecthomas/log4go"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/alpha/alpha-server/model"
	"gitlab.com/alpha/alpha-server/utils"
)

const TableEmp = "employee"

func (app *App) CreateEmployee(props *model.Props) (*model.Props, *model.AppError) {
	if ruser := <-app.Srv.Store.Item().Create(TableEmp, props); ruser.Err != nil {
		log4go.Error(utils.T("api.emp.create_employee.save.error"))
		return nil, ruser.Err
	} else {
		return ruser.Data.(*model.Props), nil
	}
}

func (app *App) ListEmployee() ([]*model.Props, *model.AppError) {
	if ruser := <-app.Srv.Store.Item().ReadAll(TableEmp, nil); ruser.Err != nil {
		log4go.Error(utils.T("api.emp.list.error"))
		return nil, ruser.Err
	} else {
		return ruser.Data.([]*model.Props), nil
	}
}

func (app *App) DeleteEmployee(id string) (*model.SuccessResponse, *model.AppError) {
	if ruser := <-app.Srv.Store.Item().Delete(TableEmp, id); ruser.Err != nil {
		log4go.Error(utils.T("api.emp.delete.error"))
		return nil, ruser.Err
	} else {
		return ruser.Data.(*model.SuccessResponse), nil
	}
}

func (app *App) GetEmployee(id string) (*model.Props, *model.AppError) {
	if ruser := <-app.Srv.Store.Item().Read(TableEmp, bson.ObjectIdHex(id)); ruser.Err != nil {
		log4go.Error(utils.T("api.emp.getemployee.error"))
		return nil, ruser.Err
	} else {
		return ruser.Data.(*model.Props), nil
	}
}
