package app

import (
	"github.com/alecthomas/log4go"

	"gitlab.com/alpha/alpha-server/model"
	"gitlab.com/alpha/alpha-server/utils"
)

func (app *App) CreateContact(props *model.Props) (*model.Props, *model.AppError) {
	if contact := <-app.Srv.Store.Item().Create(model.OBJECTS_TABLE, props); contact.Err != nil {
		log4go.Error(utils.T("api.crm.create_contact.save.error"))
		return nil, contact.Err
	} else {
		return contact.Data.(*model.Props), nil
	}
}
