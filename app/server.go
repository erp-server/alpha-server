package app

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	l4g "github.com/alecthomas/log4go"
	"github.com/gorilla/mux"
	"github.com/rs/cors"

	"gitlab.com/alpha/alpha-server/store"
	"gitlab.com/alpha/alpha-server/utils"
)

type Server struct {
	Store      store.Store
	Router     *mux.Router
	Server     *http.Server
	ListenAddr *net.TCPAddr
}

func (a *App) InitStores() {
	a.Srv.Store = store.NewLayeredStore(&a.Config().DataBaseSettings)
}

func (a *App) StartServer() {
	cc := cors.AllowAll()
	a.Srv.Server = &http.Server{
		Handler: cc.Handler(a.Srv.Router),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}

	listener, err := net.Listen("tcp", *a.Config().ServiceSettings.ListenAddress)
	if err != nil {
		l4g.Critical(utils.T("api.server.start_server.starting.critical"), err)
		return
	}
	a.Srv.ListenAddr = listener.Addr().(*net.TCPAddr)

	l4g.Info(utils.T("api.server.start_server.listening.info"), listener.Addr().String())

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := a.Srv.Server.Serve(listener); err != nil {
			l4g.Critical(utils.T("api.server.start_server.starting.critical"), err)
			time.Sleep(time.Second)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	a.Srv.Server.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("Server shutting down")
	os.Exit(0)
}
