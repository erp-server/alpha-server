package app

type Option func(a *App)

func ConfigFile(file string) Option {
	return func(a *App) {
		a.configFile = file
	}
}
