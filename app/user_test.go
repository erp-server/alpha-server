package app

import (
	"log"
	"testing"

	"gopkg.in/mgo.v2/bson"

	l4g "github.com/alecthomas/log4go"

	"gitlab.com/alpha/alpha-server/model"
	"gitlab.com/alpha/alpha-server/utils"
)

func setup() *App {
	l4g.Global["stdout"].Level = l4g.ERROR

	options := []Option{ConfigFile("config.json")}
	a, _ := NewApp(options...)
	*utils.Cfg.DataBaseSettings.DatabaseName = "test"
	if utils.TranslationsPreInit() != nil {
		log.Println("error")
	}
	a.InitStores()
	return a
}

func getTestUser() *model.User {
	return &model.User{
		Id:               bson.ObjectIdHex("5c4ae934c10c23147c4b71e9"),
		PR_USERNAME:      "sd",
		PR_PASSWORD:      "a",
		PR_EMAIL_ADDRESS: "snehal@kopano.com",
		PR_NICKNAME:      "snehal",
		PR_DISPLAYNAME:   "snehal dangroshiya",
		PR_FIRSTNAME:     "snehal",
		PR_LASTNAME:      "dangroshiya",
		PR_ROLES:         "employee_user",
		PR_LOCALE:        "en",
	}
}

func TestCreateUser(t *testing.T) {
	a := setup()
	// valid user test
	user, err := a.CreateUser(getTestUser())
	if err != nil && user == nil {
		t.Errorf("User.GetUserFromLoginID returned error: %v", err.Message)
	}
}

func TestGetUserFromLoginID(t *testing.T) {
	a := setup()

	// valid user test
	user, err := a.GetUserFromLoginID("sd")
	if err != nil {
		t.Errorf("User.GetUserFromLoginID returned error: %v", err.Message)
	}

	want := getTestUser()
	if model.ComparePassword(user.PR_PASSWORD, "a") == false {
		t.Errorf("User.GetUserFromLoginID returned %+v, want %+v", user, want)
	}
}

func TestGetUserByID(t *testing.T) {
	a := setup()

	// valid user test
	user, err := a.GetUserByID("5c4ae934c10c23147c4b71e9")
	if err != nil || user == nil {
		t.Errorf("User.GetUserByID returned error: %v", err.Message)
	}
}

func TestGetUsers(t *testing.T) {
	a := setup()

	// valid user test
	user, err := a.GetUsers()
	if err != nil || user == nil {
		t.Errorf("User.GetUsers returned error: %v", err.Message)
	}
}

// func TestGetContext(t *testing.T) {
// 	a := setup()
//
// 	// valid user test
// 	_, err := a.GetContext("5c4ae934c10c23147c4b71e9")
//
// 	if err != nil {
// 		t.Errorf("User.GetContext returned error: %v", err.Message)
// 	}
// }

func TestDeleteUserById(t *testing.T) {
	a := setup()

	// valid user test
	resp, err := a.DeleteUserById("5c4ae934c10c23147c4b71e9")
	if err != nil {
		t.Errorf("User.DeleteUserById returned error: %v", err.Message)
	}

	if resp.Success == false {
		t.Errorf("User.DeleteUserById returned error: %v", resp.Success)
	}
}
