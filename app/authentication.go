package app

import (
	"log"
	"net/http"

	"gitlab.com/alpha/alpha-server/model"
)

func (a *App) authenticateUser(user *model.User, password, token string) (*model.User, *model.AppError) {
	if err := a.CheckPasswordAndAllCriteria(user, password, token); err != nil {
		err.StatusCode = http.StatusUnauthorized
		return user, err
	} else {
		return user, nil
	}
}

func (a *App) CheckPasswordAndAllCriteria(user *model.User, password string, token string) *model.AppError {
	if err := CheckUserAdditionalAuthenticationCriteria(user, token); err != nil {
		return err
	}

	if err := a.checkUserPassword(user, password); err != nil {
		return err
	}

	return nil
}

func CheckUserAdditionalAuthenticationCriteria(user *model.User, token string) *model.AppError {
	//TODO: implement email verification functionality for the user verification.
	/**
	if user-email-verification {

	}*/

	if err := checkUserNotDisabled(user); err != nil {
		return err
	}

	if err := checkUserLoginAttempts(user); err != nil {
		return err
	}

	return nil
}

func (a *App) checkUserPassword(user *model.User, password string) *model.AppError {
	log.Println("password:-", user.PR_PASSWORD)
	if !model.ComparePassword(user.PR_PASSWORD, password) {
		user.PR_LOGIN_ATTEMPTS += 1
		if result := <-a.Srv.Store.User().Update(user); result.Err != nil {
			return result.Err
		}
		return model.NewAppError("checkUserPassword", "api.user.check_user_password.invalid.app_error", nil, "user_id="+user.Id.Hex(), http.StatusUnauthorized)
	} else {
		user.PR_LOGIN_ATTEMPTS = 0
		if result := <-a.Srv.Store.User().Update(user); result.Err != nil {
			return result.Err
		}
	}
	return nil
}

func checkUserNotDisabled(user *model.User) *model.AppError {
	if user.PR_DELETE_AT > 0 {
		err := model.NewAppError("checkUserNotDisabled", "api.authenticate.user.disabled.app_error", nil, "", http.StatusBadRequest)
		return err
	}
	return nil
}

func checkUserLoginAttempts(user *model.User) *model.AppError {
	// TODO: After implementing email verification thing implement
	// maximum attempts to user login functionality
	if user.PR_LOGIN_ATTEMPTS > 3 {
		return nil
	}
	return nil
}
