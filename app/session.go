package app

import "gitlab.com/alpha/alpha-server/model"

func (a *App) CreateSession(session *model.Session) (*model.Session, *model.AppError) {
	result := <-a.Srv.Store.Session().Create(session)
	if result.Err != nil {
		return nil, result.Err
	}
	return result.Data.(*model.Session), nil
}

func (a *App) GetSession(token string) (*model.Session, *model.AppError) {
	result := <-a.Srv.Store.Session().Get(token)
	if result.Err != nil {
		return nil, result.Err
	}
	return result.Data.(*model.Session), nil
}
