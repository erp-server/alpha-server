package app

import (
	"sync/atomic"

	l4g "github.com/alecthomas/log4go"
	"github.com/gorilla/mux"

	"gitlab.com/alpha/alpha-server/store"
	"gitlab.com/alpha/alpha-server/utils"
)

type App struct {
	Srv   *Server
	Store store.Store

	config     atomic.Value
	configFile string
	configPath string
}

func NewApp(options ...Option) (*App, error) {
	a := &App{
		Srv: &Server{
			Router: mux.NewRouter(),
		},
		configFile: "config.json",
	}

	for _, option := range options {
		option(a)
	}

	if utils.T == nil {
		if err := utils.TranslationsPreInit(); err != nil {
			return nil, err
		}
	}

	a.LoadConfig(a.configFile)
	a.Config().SetDefaults()
	l4g.Info(utils.T("api.server.new_server.init.info"))

	return a, nil
}
